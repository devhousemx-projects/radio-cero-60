
<!DOCTYPE html>
<!--[if lt IE 7 ]><html dir="ltr" lang="es-MX" class="no-js ie ie6 lte7 lte8 lte9" ><![endif]-->
<!--[if IE 7 ]><html dir="ltr" lang="es-MX" class="no-js ie ie7 lte7 lte8 lte9" ><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="es-MX" class="no-js ie ie8 lte8 lte9" ><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="es-MX" class="no-js ie ie9 lte9" ><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="es-MX" class="no-js" ><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cero:60 Agencia informativa universitaria - UDEMORELIA</title>
	<meta name="description" content="Cero:60 Agencia informativa universitaria">
	<meta name="author" content="UDEM">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<link rel="stylesheet" href="js/vendor/slick/slick.css">
	<link rel="stylesheet" href="js/vendor/slick/slick-theme.css">
	<link rel="stylesheet" href="css/master.css">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	<header class="container-fluid">
		<div class="container">
			<div class="row">
				<div id="date-statics" class="col-sm-4">
					<div>Miércoles, 01 de noviembre de 2018</div>
					<div class="social-links">
						<a href="" class="icon icon-facebook"></a>
						<a href="" class="icon icon-youtube"></a>
						<a href="" class="icon icon-twitter"></a>
					</div>
					<div>Morelia, Michoacán: 15° C</div>
				</div>
				<div class="col-xs-7 col-sm-4">
					<img src="images/lg-cero60.png" alt="" id="logo">
				</div>
				<div class="col-xs-5 col-sm-4">
					<a href="" class="btn btn-default"><span class="icon icon-play"></span> Radio - titulo <span class="icon icon-sound"></span></a>
					<a href="category.php" class="btn btn-nav"><span class="icon icon-menu"></span> Categorías</a>
				</div>
			</div>			
		</div>
		<div id="date-statics-bar" class="row">
			<div class="col-xs-7">Miércoles 01 de noviembre de 2018</div>
			<div class="col-xs-5">Morelia, Michoacán: 15°C</div>
		</div>
	</header>
	<div id="content">
		<div class="container">
			<div class="row">
				<h1 class="section-title">Entrevistas</h1>
				<div class="hidden-xs mrgT10 row">
					<div class="col-sm-6">
						<div class="camp">
							<img src="images/camp/default-468x60.jpg" alt="">
						</div>						
					</div>
					<div class="col-sm-6">
						<div class="camp">
							<img src="images/camp/default-468x60.jpg" alt="">
						</div>						
					</div>
				</div>
				<div id="single-category-featuring" class="row">
					<div class="col-sm-9">
						<div class="new-prev">
							<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
								<figcaption>
									<div class="new-title">Título para la nota</div>
									<div class="new-intro">Pie de nota</div>
								</figcaption>
							</figure>
							<div class="new-date">Domingo 09 de noviembre del 2017</div>
						</div>						
					</div>
					<div id="category-featuring-media" class="media-wp col-sm-3 hidden-xs">
						<div class="media-wp media-video">
							<a href="#" class="btn-media"><img src="images/lg-video.png" alt=""></a>
						</div>
						<div class="media-wp media-radio">
							<a href="#" class="btn-media"><img src="images/lg-radio.png" alt=""></a>
						</div>
					</div>
				</div>
				<div class="camp visible-xs visible-sm">
					<img src="images/camp/default-468x60.jpg" alt="">
				</div>
				<div class="row">
					<div class="col-md-9">
						<div class="new-prev col-sm-6">
							<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
								<figcaption>
									<div class="new-title">Título para la nota</div>
									<div class="new-intro">Pie de nota</div>
								</figcaption>
							</figure>
							<div class="new-date">Domingo 09 de noviembre del 2017</div>
						</div>
						<div class="new-prev col-sm-6">
							<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
								<figcaption>
									<div class="new-title">Título para la nota</div>
									<div class="new-intro">Pie de nota</div>
								</figcaption>
							</figure>
							<div class="new-date">Domingo 09 de noviembre del 2017</div>
						</div>
						<div class="new-prev col-sm-6">
							<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
								<figcaption>
									<div class="new-title">Título para la nota</div>
									<div class="new-intro">Pie de nota</div>
								</figcaption>
							</figure>
							<div class="new-date">Domingo 09 de noviembre del 2017</div>
						</div>
						<div class="new-prev col-sm-6">
							<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
								<figcaption>
									<div class="new-title">Título para la nota</div>
									<div class="new-intro">Pie de nota</div>
								</figcaption>
							</figure>
							<div class="new-date">Domingo 09 de noviembre del 2017</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="camp square">
							<img src="images/camp/default-300x250.jpg" alt="">
						</div>
						<div class="camp square">
							<img src="images/camp/default-300x250.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="camp visible-xs visible-sm">
					<img src="images/camp/default-468x60.jpg" alt="">
				</div>
				<div class="row visible-md visible-lg">
					<div class="col-sm-5 col-sm-offset-1">
						<div class="camp">
							<img src="images/camp/default-392x72.jpg" alt="">
						</div>
					</div>
					<div class="col-sm-5">
						<div class="camp">
							<img src="images/camp/default-392x72.jpg" alt="">
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<footer class="container">
		<div class="footer-data row">
			<div id="footer-legal" class="row">
				contacto / política de privacidad / avisos legales
			</div>
			<div id="footer-logos" class="row">
				<div class="col-xs-6 col-sm-4">
					<img src="images/lg-udemorelia-white.png" alt="">
				</div>
				<div class="col-xs-6 col-sm-4 col-sm-offset-4">
					<img src="images/lg-periodismo.png" alt="">				
				</div>
			</div>
			<div id="footer-address">
				Universidad de Morelia SC<br />
				Dirección de Periodismo<br />
				01 800-509-9422 / (443) 317-7771 ext. 207<br />
				Fray Antonio de Lisboa #22 Col. Cinco de Mayo.<br />
				Morelia, Michoacán.
			</div>			
		</div>
		<div class="row">
			Derechos Reservados &copy; 2016 Universidad de Morelia SC / Escuela de Periodismo.
		</div>
	</footer>
	<script src="js/jquery.min.js"></script>
	<script src="js/vendor/slick/slick.min.js"></script>
	<script>
		$(document).ready(function() {
			$('.slider').slick({
				dots: true,
			});
		})
	</script>
</body>
</html>