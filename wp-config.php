<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'devhouse_cero60');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '%lt{#B*ttKnENHK6A6]z/yUYl.vxz>$M]JQk*&infW[*JH9pw#0.usoMr3ms=h.P');
define('SECURE_AUTH_KEY', 'clXTw %d+uA*AKGVTAJKx[fmo(`y1B+Lx:KvYQ~>:JXR5eHY;4h$Jd>e6:$;`yji');
define('LOGGED_IN_KEY', '6WhC@b_g$fiHXzmrk/NC4aq5Tw,k9B3GCnsl0#|Si`tGuX27y%9n{(J(YgQw-Tte');
define('NONCE_KEY', ']&kZnlo>.c}:FQSrjH*86KJo#N<n_SL_+m@@Tj;so_oTKn0:QcWoteUJ$Y[_NDK<');
define('AUTH_SALT', '_BbMzJ-TUVG}4{A^C;|eOoqC5W{?_LCCT0Qm^H9<7|an`*=a>=iLF,6),X&jy|e,');
define('SECURE_AUTH_SALT', 'hOL;/6^;n`N%^saRrr44t8 4=bCf-?_c;%p9z{B=6 /VtJ=~8{gVK?X(Gn`&iFN$');
define('LOGGED_IN_SALT', ';a;B~HeL+q+#o{5S(heL@Q>(:C{IDY6x+vlD{}!c%1khV=lm}z0fQ}PA_x38;wC[');
define('NONCE_SALT', '?#.[u&/P<)82x>)MBAG^E%+/lsv6aaMIt` hC,ufxvSL,_W7k_?65AU~so#Xtfb@');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'ai_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

