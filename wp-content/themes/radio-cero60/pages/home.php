<?php 
/*
Template Name: Home
*/
?>

<?php get_header(); ?>
<div class="container-fluid">
			<div id="home-slider" class="row">
				<div class="slider-wp col-md-10 col-md-offset-1">
					<div class="slider" data-slider-dots="true">
						<?php 
							$original_query = $wp_query;
							$wp_query = null;
							$args = array(
								'posts_per_page' => 0, 
								'order' => 'DESC',
								'orderby' => 'date',
								'category_name' => 'principal'
							);
							$wp_query = new WP_Query( $args );
				      	?>
			        	<?php while (have_posts()) : the_post(); ?>
							<a href="<?php echo get_permalink(get_the_ID()); ?>" class="new-cover" style="background-image:url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full'); ?>')">
								<div class="caption visible-md visible-lg">
									<h2><?php the_title() ?></h2>
									<p><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
								</div>
							</a>
						<?php endwhile; ?>
				      	<?php 
							$wp_query = null;
							$wp_query = $original_query;
							wp_reset_postdata();				      	
							wp_reset_query();
				      	?>


						<?php 
							$original_query = $wp_query;
							$wp_query = null;
							$args = array(
								'posts_per_page' => 5, 
								'order' => 'DESC',
								'orderby' => 'date',
								//'tag' => 'principal'
							);
							$wp_query = new WP_Query( $args );
				      	?>
			        	<?php while (have_posts()) : the_post(); ?>
							<a href="<?php echo get_permalink(get_the_ID()); ?>" class="new-cover" style="background-image:url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full'); ?>')">
								<div class="caption visible-md visible-lg">
									<h2><?php the_title() ?></h2>
									<p><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
								</div>
							</a>
						<?php endwhile; ?>
				      	<?php 
							$wp_query = null;
							$wp_query = $original_query;
							wp_reset_postdata();				      	
							wp_reset_query();
				      	?>
					</div>
					<span class="slick-arrow slick-prev icon-angle-left"></span>
					<span class="slick-arrow slick-next  icon-angle-right"></span>				
				</div>
			</div>			
		</div>
		<div class="container">
			<div class="row">
				<div class="row">
					<div class="camp col-xs-12 col-md-6">
						<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">-->
						<?php echo adrotate_group(1); ?>
					</div>
					<div class="camp col-xs-12 col-md-6 visible-md visible-lg">
						<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">-->
						<?php echo adrotate_group(2); ?>
					</div>					
				</div>
				<div id="home-media-access" class="clearfix">
					<div class="row">

						<div class="col-md-4 visible-md visible-lg">
							<div class="category-featuring">
								<h2 class="subsection-title">Programas</h2>
								<ul class="row">
									<?php 
										$args = array(
											'category_name' => 'programas',
											//'category__not_in' => array(get_cat_ID('Prensa'), get_cat_ID('Eventos'), get_cat_ID('Bolsa de Trabajo')),
											'posts_per_page'=> 1,        
											'post_type'=> 'post',         
											'post_status' => 'publish', 	
											'orderby' => 'post_date', 
											'order' => 'DESC',							
										);					
										$wp_query = new WP_Query( $args );

										while (have_posts()) : the_post();
									?>
									<li class="col-sm-12">
										<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
									</li>
									<?php endwhile; ?> 
							      	<?php 
										$wp_query = null;
										$wp_query = $original_query;
										wp_reset_postdata();				      	
										wp_reset_query();
							      	?>
								</ul>
							</div>
						</div>
						<div class="col-md-4 visible-md visible-lg">
							<div class="category-featuring">
								<h2 class="subsection-title">Podcasts</h2>
								<ul class="row">
									<?php 
										$args = array(
											'category_name' => 'podcasts',
											//'category__not_in' => array(get_cat_ID('Prensa'), get_cat_ID('Eventos'), get_cat_ID('Bolsa de Trabajo')),
											'posts_per_page'=> 1,        
											'post_type'=> 'post',         
											'post_status' => 'publish', 	
											'orderby' => 'post_date', 
											'order' => 'DESC',							
										);					
										$wp_query = new WP_Query( $args );

										while (have_posts()) : the_post();
									?>
									<li class="col-sm-12">
										<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
									</li>
									<?php endwhile; ?> 
							      	<?php 
										$wp_query = null;
										$wp_query = $original_query;
										wp_reset_postdata();				      	
										wp_reset_query();
							      	?>
								</ul>
							</div>
						</div>
						<div class="col-md-4 visible-md visible-lg">
							<div class="category-featuring">
								<h2 class="subsection-title">Reportajes</h2>
								<ul class="row">
									<?php 
										$args = array(
											'category_name' => 'reportajes',
											//'category__not_in' => array(get_cat_ID('Prensa'), get_cat_ID('Eventos'), get_cat_ID('Bolsa de Trabajo')),
											'posts_per_page'=> 1,        
											'post_type'=> 'post',         
											'post_status' => 'publish', 	
											'orderby' => 'post_date', 
											'order' => 'DESC',							
										);					
										$wp_query = new WP_Query( $args );

										while (have_posts()) : the_post();
									?>
									<li class="col-sm-12">
										<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
									</li>
									<?php endwhile; ?> 
							      	<?php 
										$wp_query = null;
										$wp_query = $original_query;
										wp_reset_postdata();				      	
										wp_reset_query();
							      	?>
								</ul>
							</div>
						</div>

						<div class="hidden-xs col-sm-5 col-md-4">
							<div class="camp square">
								<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-300x250.jpg'; ?>" alt="">-->
								<?php echo adrotate_group(4); ?>
							</div>
						</div>						
					</div>
				</div>
				<!--<div class="camp">
					<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">
				</div>-->
				<div class="category-explorer visible-xs">
					<?php 
						$args = array(
							'category' => get_cat_ID('Entrevistas'), 						 
							//'category__not_in' => array(get_cat_ID('Prensa'), get_cat_ID('Eventos'), get_cat_ID('Bolsa de Trabajo')),
							'posts_per_page'=> 2,        
							'post_type'=> 'post',         
							'post_status' => 'publish', 	
							'orderby' => 'post_date', 
							'order' => 'DESC',							
						);					
						$wp_query = new WP_Query( $args );

						while (have_posts()) : the_post();
					?>
					<div class="new-prev">
						<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
					</div>
					<?php endwhile; ?> 
			      	<?php 
						$wp_query = null;
						$wp_query = $original_query;
						wp_reset_postdata();				      	
						wp_reset_query();
			      	?>
				</div>
				<div class="camp visible-xs">
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-392x72.jpg'; ?>" alt="">-->
					<?php echo adrotate_group(5); ?>
				</div>
				<div class="category-featuring">
					<h2 class="subsection-title">Reportaje</h2>
					<ul class="row">
						<?php 
							$args = array(
								'category_name' => 'reportaje',
								//'category__not_in' => array(get_cat_ID('Prensa'), get_cat_ID('Eventos'), get_cat_ID('Bolsa de Trabajo')),
								'posts_per_page'=> 3,        
								'post_type'=> 'post',         
								'post_status' => 'publish', 	
								'orderby' => 'post_date', 
								'order' => 'DESC',							
							);					
							$wp_query = new WP_Query( $args );

							while (have_posts()) : the_post();
						?>
						<li class="col-sm-4">
							<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
						</li>
						<?php endwhile; ?> 
				      	<?php 
							$wp_query = null;
							$wp_query = $original_query;
							wp_reset_postdata();				      	
							wp_reset_query();
				      	?>
					</ul>
				</div>
				<div class="row visible-md visible-lg">
					<div class="col-sm-5 col-sm-offset-1">
						<div class="camp">
							<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-392x72.jpg'; ?>" alt="">-->
							<?php echo adrotate_group(6); ?>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="camp">
							<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-392x72.jpg'; ?>" alt="">-->
							<?php echo adrotate_group(7); ?>
						</div>
					</div>
				</div>
			</div>		
		</div>
<?php get_footer(); ?>