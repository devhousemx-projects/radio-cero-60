<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cero60
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
<style>
.single-new .new-cover{background-position:center center;background-size:cover;}
</style>
	<?php wp_head(); ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119541289-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	 
	  gtag('config', 'UA-119541289-1');
	</script>

</head>

<body <?php body_class(); ?>>
<header class="container-fluid">
		<div class="container">
			<div class="header-wp row">
				<div id="date-statics" class="col-sm-4">
					<div><?php echo date_i18n('l, j F, Y'); ?></div>
					<div class="social-links">
						<a href="https://www.facebook.com/pages/Agencia-Informativa-Cero60/1521211821500388" target="_blank" class="icon icon-facebook"></a>
						<a href="https://www.youtube.com/channel/UC4EVDLs1WTuHj46jQV2FnXg/feed?spfreload=10" target="_blank" class="icon icon-youtube"></a>
						<a href="https://twitter.com/AgenciaCero60" target="_blank" class="icon icon-twitter"></a>
					</div>
					<div>Morelia, Michoacán: 15° C</div>
				</div>
				<div class="col-xs-6 col-sm-4">
					<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/images/lg-cero60.png'; ?>" alt="" id="logo"></a>
				</div>
				<div class="col-xs-6 col-sm-4">
					<iframe id="radio-iframe" src="https://www.ustream.tv/embed/22124593?html5ui" style="border: 0 none transparent;"  webkitallowfullscreen allowfullscreen frameborder="no"></iframe>
					<div id="btn-radio" class="btn btn-default">
						<span class="btn-radio-play icon icon-play pull-left"></span>
						<span class="radio-static">Radio</span> <span class="radio-label"></span> <span class="radio-time"></span>
						<span class="btn-radio-mute icon icon-volume-medium pull-right"></span>
					</div>
					<button class="btn btn-nav"><span class="icon icon-menu"></span> Categorías</button>
				</div>
				<nav>
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
					?>
				</nav>
			</div>			
		</div>
		<div id="date-statics-bar" class="row">
			<div class="col-xs-7"><?php echo date_i18n('l, j F, Y'); ?></div>
			<div class="col-xs-5">Morelia, Michoacán: 15°C</div>
		</div>
	</header>
	<div id="content">
