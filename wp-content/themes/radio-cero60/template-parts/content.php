<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cero60
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( 'post' === get_post_type() ) : ?>
		<?php if ( is_singular() ) : ?>
			<div class="container-fluid">
				<div class="row">
					<figure class="new-cover" style="background-image:url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full'); ?>')">
					</figure>
				</div>
			</div>
			<div class="container">		
		<?php else: ?>
			<a href="<?php echo get_permalink(get_the_ID()); ?>">
				<figure class="new-cover">
					<span style="background-image:url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full'); ?>')"></span>
					<figcaption>
						<?php the_title( '<h3 class="new-title">', '</h3>' ); ?>
						<p class="new-intro"><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
					</figcaption>
				</figure>				
			</a>
		<?php endif; ?>

		<?php if ( is_singular() ) : 
			//the_post();
            $gallery = get_post_gallery();
            $content = strip_shortcode_gallery( get_the_content() );
		?>
				<div class="row">
					<div class="col-md-10">
						<div class="visible-xs mrgT10">
							<div class="camp">
								<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">-->
								<?php echo adrotate_group(1); ?>
							</div>			
						</div>
						<?php the_title( '<h1 class="new-title">', '</h1>' ); ?>
						<div class="new-data"><?php foreach((get_the_category()) as $category) echo $category->cat_name!='Slider Principal' ? $category->cat_name . ' ' : ''; ?> / <?php the_date('d M, Y', '', ''); ?> / <?php cero60_posted_by(); ?></div>
						<div class="new-content">
							<?php 
								the_content( sprintf(
									__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
									get_the_title()
								) );
							 ?>
						</div>
						<?php if ( get_post_gallery() ) : 
					            $gallery = get_post_gallery( get_the_ID(), false ); 
					            $ids = explode( ",", $gallery['ids'] );
					    ?>
					            <div id="new-slider" class="row">
									<div class="col-md-12">
										<div class="slider-wp">
											<div class="slider">
						    					<?php foreach( $ids as $src ) : ?>
												<a href="#" class="new-cover" style="background-image:url('<?php echo wp_get_attachment_url($src); ?>')">
													<!--<div class="caption visible-md visible-lg">
														<h2>Título para el carrusel</h2>
														<p>Pie de nota</p>
													</div>-->
												</a>
						                		<?php endforeach; ?>
											</div>					
					                		<span class="slick-arrow slick-prev icon-angle-left"></span>
					                		<span class="slick-arrow slick-next  icon-angle-right"></span>
										</div>
									</div>
								</div>
					    <?php endif; ?>						
					</div>
					<div class="col-md-2">
						<div class="visible-md visible-lg mrgT10">
							<div class="camp">
								<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-160x600.png'; ?>" alt="">-->
								<?php echo adrotate_group(9); ?>
							</div>			
						</div>
					</div>
								
					
				</div>
			</div> <!-- close singular article container -->
		<?php endif; ?>

	<?php endif; ?>


	<?php //cero60_entry_footer(); ?>

</article><!-- #post-<?php the_ID(); ?> -->
