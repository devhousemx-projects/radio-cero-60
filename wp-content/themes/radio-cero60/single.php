<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cero60
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main single-new">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			//the_post_navigation();

			/*
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			*/

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		<div class="container">
			<div class="category-featuring row">
				<div class="col-sm-12">
					<h2 class="subsection-title">Puede leer también:</h2>
					<ul class="row">
						<?php 
							$args = array(
								//'category' => get_cat_ID('Entrevistas'), 						 
								//'category__not_in' => array(get_cat_ID('Prensa'), get_cat_ID('Eventos'), get_cat_ID('Bolsa de Trabajo')),
								'posts_per_page'=> 2,        
								'post_type'=> 'post',         
								'post_status' => 'publish', 	
								'orderby' => 'post_date', 
								'order' => 'DESC',							
							);					
							$wp_query = new WP_Query( $args );

							while (have_posts()) : the_post();
						?>
						<li class="col-sm-3">
							<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
						</li>
						<?php endwhile; ?> 
				      	<?php 
							$wp_query = null;
							$wp_query = $original_query;
							wp_reset_postdata();				      	
							wp_reset_query();
				      	?>
					</ul>					
				</div>
			</div>			
		</div>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
