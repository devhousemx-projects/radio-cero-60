<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cero60
 */

get_header();
?>

<div class="container">
	<div class="row">
		<?php the_archive_title( '<h1 class="section-title">', '</h1>' ); ?>
		<div class="hidden-xs mrgT10 row">
			<div class="col-sm-6">
				<div class="camp">
					<?php echo adrotate_group(1); ?>
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">-->
				</div>						
			</div>
			<div class="col-sm-6">
				<div class="camp">
					<?php echo adrotate_group(2); ?>
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">-->
				</div>						
			</div>
		</div>
		<?php $i=1; ?>
		<?php if ( have_posts() ) : ?>
		<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
		?>
				<?php if($i++==1): ?>
					<div id="single-category-featuring" class="row">
						<div class="col-sm-9">
							<div class="new-prev">
								<a href="<?php echo get_permalink(get_the_ID()); ?>">
									<figure class="new-cover" style="background-image:url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full'); ?>')">
										<figcaption>
											<?php the_title( '<div class="new-title">', '</div>' ); ?>
											<p class="new-intro"><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
										</figcaption>
									</figure>
									<div class="new-date"><?php the_date('l, j F, Y'); ?></div>
								</a>
							</div>						
						</div>
						<div id="category-featuring-media" class="media-wp col-sm-3 hidden-xs">
							<div class="media-wp media-video">
								<a href="#" class="btn-media"><img src="<?php echo get_template_directory_uri().'/images/lg-video.png'; ?>" alt=""></a>
							</div>
							<div class="media-wp media-radio">
								<a href="#" class="btn-media"><img src="<?php echo get_template_directory_uri().'/images/lg-radio.png'; ?>" alt=""></a>
							</div>
						</div>
					</div>
					<div class="camp visible-xs visible-sm">
						<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">
					</div>
					<div class="row">
						<div class="category-explorer col-md-9">
				<?php else: ?>
					<div class="new-prev col-sm-6">
						<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
					</div>
				<?php endif; ?>
		<?php endwhile; ?>
				<!--<div class="new-prev col-sm-6">
					<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
						<figcaption>
							<div class="new-title">Título para la nota</div>
							<div class="new-intro">Pie de nota</div>
						</figcaption>
					</figure>
					<div class="new-date">Domingo 09 de noviembre del 2017</div>
				</div>
				<div class="new-prev col-sm-6">
					<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
						<figcaption>
							<div class="new-title">Título para la nota</div>
							<div class="new-intro">Pie de nota</div>
						</figcaption>
					</figure>
					<div class="new-date">Domingo 09 de noviembre del 2017</div>
				</div>
				<div class="new-prev col-sm-6">
					<figure class="new-cover" style="background-image:url('images/img-default.jpg')">
						<figcaption>
							<div class="new-title">Título para la nota</div>
							<div class="new-intro">Pie de nota</div>
						</figcaption>
					</figure>
					<div class="new-date">Domingo 09 de noviembre del 2017</div>
				</div>-->
				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-6 pull-left"><?php previous_posts_link( '&#8592; Recientes' ); ?></div>
						<div class="col-xs-6 text-right pull-right"><?php next_posts_link( 'Anteriores &#8594;' ); ?></div>						
					</div>
				</div>	
			</div>
			<div class="col-md-3">
				<div class="camp square">
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-300x250.jpg'; ?>" alt="">-->
					<?php echo adrotate_group(4); ?>
				</div>
				<div class="camp square">
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-300x250.jpg'; ?>" alt="">-->
					<?php echo adrotate_group(8); ?>
				</div>
			</div>
		</div>
		<div class="camp visible-xs visible-sm">
			<img src="<?php echo get_template_directory_uri().'/images/camp/default-468x60.jpg'; ?>" alt="">
		</div>
		<div class="row visible-md visible-lg">
			<div class="col-sm-5 col-sm-offset-1">
				<div class="camp">
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-392x72.jpg'; ?>" alt="">-->
					<?php echo adrotate_group(6); ?>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="camp">
					<!--<img src="<?php echo get_template_directory_uri().'/images/camp/default-392x72.jpg'; ?>" alt="">-->
					<?php echo adrotate_group(7); ?>
				</div>
			</div>
		</div>
		<?php else: ?>
		
			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
	</div>	
</div>


<?php
//get_sidebar();
get_footer();
