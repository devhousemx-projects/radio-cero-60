<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cero60
 */

?>

	</div><!-- #content -->
	<footer class="container">
		<div class="footer-data row">
			<div id="footer-legal" class="row">
				contacto / política de privacidad / avisos legales
			</div>
			<div id="footer-logos" class="row">
				<div class="col-xs-6 col-sm-4">
					<img src="<?php echo get_template_directory_uri().'/images/lg-udemorelia-white.png'; ?>" alt="">
				</div>
				<div class="col-xs-6 col-sm-4 col-sm-offset-4">
					<img src="<?php echo get_template_directory_uri().'/images/lg-periodismo.png'; ?>" alt="">				
				</div>
			</div>
			<div id="footer-address">
				Universidad de Morelia SC<br />
				Dirección de Periodismo<br />
				01 800-509-9422 / (443) 317-7771 ext. 207<br />
				Fray Antonio de Lisboa #22 Col. Cinco de Mayo.<br />
				Morelia, Michoacán.
			</div>			
		</div>
		<div class="row">
			Derechos Reservados &copy; 2016 Universidad de Morelia SC / Escuela de Periodismo.
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
