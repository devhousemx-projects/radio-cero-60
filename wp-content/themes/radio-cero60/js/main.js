$(document).ready(function() {
	$('html').click(function() {
        $('header nav:visible').slideUp();
    });
	$('.btn-nav').on('click',function(e){
		e.preventDefault();
		e.stopPropagation();
		$('header nav').stop(true,false).slideToggle();
	});

	var viewer = UstreamEmbed("radio-iframe");
	var $radio = $('#btn-radio');
	String.prototype.toTime = function () {
	    var sec_num = parseInt(this, 10); // don't forget the second param
	    var hours   = Math.floor(sec_num / 3600);
	    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	    if (hours   < 10) {hours   = "0"+hours;}
	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}
	    return hours+':'+minutes+':'+seconds;
	}
	var liveUpdate = null;
	function startInterval(){
		stopInterval();
		liveUpdate = setInterval(function(){
			viewer.getProperty('progress', function (progress) {
				if(progress==0){
					$radio.find('.radio-label').html(' EN VIVO');
					$radio.find('.radio-time').html("");
				}
				else{
					$radio.find('.radio-label').html(" (OFFLINE) - ");					
					$radio.find('.radio-time').html(progress.toString().toTime());
				}
			});
		},1000);
	};
	function stopInterval(){
		clearInterval(liveUpdate);
	};
	viewer.addListener('playing', function(eventType,playing){
		if(playing)
			startInterval();
		else{
			stopInterval();
		}
	});
	$('.btn-radio-play').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		if($(this).hasClass('active'))
			viewer.callMethod('play');
		else
			viewer.callMethod('pause');
	});
	$('.btn-radio-mute').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		if($(this).hasClass('active'))
			viewer.callMethod('volume', 0);
		else
			viewer.callMethod('volume', 100);
	});

	if($('.slider').length)
		$('.slider').slick({
			dots: $(this).data('slider-dots'),
			prevArrow: $('.slick-prev'),
			nextArrow: $('.slick-next')
		});
})